#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

alias "ls"="/usr/bin/ls --color=auto --quoting-style=literal"
alias "ll"="ls -l"
alias "nocomment"="grep -v -e \"^[[:blank:]]*#\" -e \"^[[:blank:]]*$\" "
alias "vi"="/usr/bin/vim"
