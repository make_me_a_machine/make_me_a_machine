__hg_ps1() {
  local format=$1
  if hg root >/dev/null 2>&1 ; then
    local branch state unpushed uncommited
    branch=$(hg branch)
    uncommited=+$(hg status | wc -l)
    state="${unpushed}${uncommited}"
    printf "${format}" "${branch} ${state}"
  fi
}

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*|linux)
    export GIT_PS1_SHOWDIRTYSTATE=1
    export GIT_PS1_SHOWSTASHSTATE=1
    export GIT_PS1_SHOWUNTRACKEDFILES=1
    export GIT_PS1_SHOWUPSTREAM=verbose
    export PS1="\
\[$bldwht\]\
\[$(if [ $USER == root ] ; then echo $txtred ; else echo $txtgrn ; fi)\]\u@\h\
\[$txtblu\] \w\
\[$txtylw\]"'$(__git_ps1 " %s")'"\
\[$txtpur\]"'$(__hg_ps1 " %s")'"\
\[$txtrst\] \
"
    ;;
esac
