[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
[ -r /usr/share/bash-completion/completions/subversion ] && . /usr/share/bash-completion/completions/subversion
[ -r /usr/share/bash-completion/completions/git ] && . /usr/share/bash-completion/completions/git
[ -r /usr/share/git/completion/git-prompt.sh ] && . /usr/share/git/completion/git-prompt.sh 
