eval $(dircolors -b)

set -o vi

export PATH=~/bin:~/.local/bin:$PATH
export EDITOR="vim"
export BROWSER="brave"
export MANPATH="$(man -w)"

alias neofetch="XDG_CURRENT_DESKTOP= /usr/bin/neofetch --w3m ~/images/avatar.png --size 40% --xoffset 40 --yoffset 40 --colors 15 12 12 12 15"
