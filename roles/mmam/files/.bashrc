#!/usr/bin/env bash

# only source profile
. /etc/profile

export DARK_LIGHT="$(cat ~/.config/DARK_LIGHT)"
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
