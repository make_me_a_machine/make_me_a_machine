#!/usr/bin/env bash

measures_dir=~/.local/share/bootcheck
measures_pcrs=${measures_dir}/pcrs
measures_lba=${measures_dir}/lba
measures_grub=${measures_dir}/grub

lba_get() {
  for lba in {0..33}
  do
    sudo dd if=/dev/nvme0n1 bs=512 count=1 skip=$lba 2>/dev/null | sha256sum | sed 's/ .*//'
  done
}

pcrs_get() {
  [ -e /dev/tpmrm0 ] && sudo /usr/bin/tpm2_pcrread || true
}

grub_get() {
  sudo test -d /boot/efi && sudo sha256sum /boot/efi/EFI/arch/grubx64.efi | sed 's/ .*//' || true
}

measure() {
  local ret_code=0
  diff ${measures_pcrs} <(pcrs_get) || ret_code=1
  diff ${measures_lba} <(lba_get) || ret_code=1
  diff ${measures_grub} <(grub_get) || ret_code=1
  return ${ret_code}
}

case "$1" in
  update)
    mkdir -p ${measures_dir}
    pcrs_get > ${measures_pcrs}
    lba_get > ${measures_lba}
    grub_get > ${measures_grub}
    bootcheck
  ;;
  display)
    measure
  ;;
  *)
    measure >/dev/null 2>&1 && root_color="#1d2021" || root_color="#d65d0e"
    bg-rand
  ;;
esac

