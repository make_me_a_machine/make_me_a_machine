#!/usr/bin/env bash

lines=25
columns=120
prompt="make your choice"
font_size=12
font="DejaVu Sans Mono for Powerline"
text=
text_lines=0
multi="+m"
multi_bind=""
preview=""
run_in_xterm=1

while [ $# -gt 0 ] ; do
  arg=$1 ; shift
  case ${arg} in
    --lines|-l) lines=$1 ; shift ;;
    --columns|-c) columns=$1 ; shift ;;
    --prompt|-p) prompt="$1"; shift ;;
    --big|-b) font_size=$((font_size+2)) ;;
    --font-size|-fs) font_size="$1"; shift ;;
    --font|-f) font="$1"; shift ;;
    --text|-t)
      text="$1"; shift
      text_lines=$(echo -e "${text}" | wc -l)
      ;;
    --multi|-m)
      multi="-m"
      multi_bind="--bind 'ctrl-a:toggle+clear-query'"
      ;;
    --preview)
      preview="--preview='$1'"
      shift
      ;;
    --nox)
      run_in_xterm=0
      ;;
    --)
      fzf_args="'$@'"
      break
      ;;
  esac
done

lines=$((lines+2))

output_file=$(mktemp)
input_file=$(mktemp)
cat > ${input_file}

# gruvbox colors
bg_color="#282828"
fg_color="#fbf1c7"
header_color="#d65d0e"
prompt_color="#d65d0e"
select_bg_color="#504945"
select_fg_color="#fabd2f"
pointer_color="#fe8019"
gutter_color="#3c3836"
hl_color="#d65d0e"
select_hl_color="#fe8019"

txt_color="\e[1;34m"
txt_reset="\e[0m"
[ -n "${text}" ] && txt_cmd="echo -e '${txt_color}${text}${txt_reset}'" || txt_cmd="true"

if [ ${run_in_xterm} -eq 1 ] ; then
  xterm \
    -class fzf \
    -T fzf \
    -geometry ${columns}x$((lines+text_lines)) \
    -fg "#1d2821" \
    -fg "#fbf1c7" \
    -fa "${font}" \
    -fs ${font_size} \
    +sb \
    -e bash -c "${txt_cmd} ; fzf \
                  --height=${lines} \
                  --header='${prompt}' \
                  --no-info \
                  --ansi \
                  --bind 'tab:replace-query' \
                  --bind 'ctrl-space:print-query' \
                  ${multi} ${multi_bind} \
                  ${preview} \
                  --color='bg:${bg_color}' \
                  --color='fg:${fg_color}' \
                  --color='header:${header_color}' \
                  --color='prompt:${prompt_color}' \
                  --color='scrollbar:${prompt_color}' \
                  --color='bg+:${select_bg_color}' \
                  --color='pointer:${pointer_color}' \
                  --color='gutter:${gutter_color}' \
                  --color='hl:${hl_color}' \
                  --color='hl+:${select_hl_color}' \
                  ${fzf_args} \
                  < ${input_file} > ${output_file}"
else
    bash -c "${txt_cmd} ; fzf \
                  --height=${lines} \
                  --header='${prompt}' \
                  --no-info \
                  --ansi \
                  --bind 'tab:replace-query' \
                  --bind 'ctrl-space:print-query' \
                  ${multi} ${multi_bind} \
                  ${preview} \
                  --color='bg:${bg_color}' \
                  --color='fg:${fg_color}' \
                  --color='header:${header_color}' \
                  --color='prompt:${prompt_color}' \
                  --color='scrollbar:${prompt_color}' \
                  --color='bg+:${select_bg_color}' \
                  --color='pointer:${pointer_color}' \
                  --color='gutter:${gutter_color}' \
                  --color='hl:${hl_color}' \
                  --color='hl+:${select_hl_color}' \
                  ${fzf_args} \
                  < ${input_file} > ${output_file}"
fi

cat ${output_file}
rm ${output_file} ${input_file}
exit
