#!/usr/bin/env bash

set -e -o pipefail

. ~/bin/libfru

playlist_select() {
  local playListDir="${HOME}/.config/mpd/playlists"
  wc -l ${playListDir}/*.m3u | sed -r 's,/.*/,,;/^ *[0-9]* total$/d;s/^( *)([0-9]*)/\1\[\2\]/;s/\.m3u$//' | \
    menu "select a playlist"
}

option_choice() {
  local options_count menu_lines
  local max_lines=15
  options_count=$(options_list | wc -l)
  [ ${options_count} -lt ${max_lines} ] && menu_lines=${options_count} || menu_lines=${max_lines}
  options_list | menu -b -l ${menu_lines} -c 50 -p "your choice"
}

options_list() {
  cat <<EOF
play album
add song
add random songs
play playlist
add current song to playlist
display current song
clear playlist
delete previous songs
playlist monitor
restart mpd
EOF
}

do_play_album() {
  local album artist
  album=$(mpc -f "%artist%/%album%" search album "" | sort -u | menu "album")
  [ -n "${album}" ] || exit
  artist=$(echo "$album" | cut -d '/' -f 1)
  album=$(echo "$album" | cut -d '/' -f 2)
  mpc clear
  mpc search artist "${artist}" album "${album}" | mpc add
  mpc play
  notify-send -a mpd "adding to mpd playlist : ${artist} - ${album}."
}

do_add_song() {
  local title
  title=$(mpc listall | sort -n | menu "track")
  [ -n "${title}" ] || exit
  echo "${title}" | mpc add
  notify-send -a mpd "adding to mpd playlist : ${title}"
}

do_add_random_songs() {
  local count=10
  mpc listall | sort -R | head -$count | mpc add
}

do_play_playlist() {
  local playList
  playList=$(playlist_select)
  if [ -n "$playList" ] ; then
    playList="$(echo $playList | sed "s/^ *\[[0-9]*\] *//").m3u"
    if [ -f ${playListDir}/"${playList}" ] ; then
      notify-send "playing '$playList'"
      mpc clear >/dev/null 2>&1
      cat ${playListDir}/"${playList}" | mpc add >/dev/null 2>&1
      mpc play >/dev/null 2>&1
    fi
  fi
}

do_add_current_song_to_playlist() {
  local curSong playList
  curSong=$(mpc current -f "%file%")
  playList=$(playlist_select)
  if [ -n "$playList" ] ; then
    playList="$(echo $playList | sed "s/^ *\[[0-9]*\] *//").m3u"
    notify-send "adding '$curSong' to '$playList'"
    echo "$curSong" >> ${playListDir}/"${playList}"
  fi
}

do_display_current_song() {
  exec notify-send -a mpd "$(mpc)"
}

do_clear_playlist() {
 exec mpc clear
}

do_delete_previous_songs() {
  local mpdIdx
  mpdIdx=$(mpc playlist | grep -n "^$(mpc current)$" | cut -d: -f1)
  [ $# -ge 1 ] && mpdIdx=$1
  mpdIdx=$(($mpdIdx-1))
  [ $mpdIdx -lt 1 ] && exit 1
  for i in $(seq 1 $mpdIdx) ; do mpc del 1 ; done
}

do_playlist_monitor() {
  exec xterm -e "watch -c -n 1 -t mpc-display" &
}

do_restart_mpd() {
  systemctl --user restart mpd.service
}

main() {
  local action=$(option_choice | tr ' ' '_')
  [ -z "${action}" ] && exit 0
  do_${action}
}

main
exit 0
