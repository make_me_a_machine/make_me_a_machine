all: configure play

play:
	ansible-playbook -i hosts playbook.yml --diff --extra-vars "continue=true" --vault-password-file ".vaultpass"

### run the playbook without sudo as root for the first run
root: playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml --diff --vault-password-file ".vaultpass"

root_continue: playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml --diff --extra-vars "continue=true" --vault-password-file ".vaultpass"

lts:
	ansible-playbook -i hosts playbook_lts.yml --diff --vault-password-file ".vaultpass" --extra-vars "boot_device=$(boot_device)"

### make dry run on all
test: configure
	ansible-playbook -i hosts playbook.yml --check --diff --extra-vars "continue=true" --vault-password-file ".vaultpass"

### make a dry run on root
root_test: playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml --check --diff --vault-password-file ".vaultpass"

root_continue_test: playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml --check --diff --extra-vars "continue=true" --vault-password-file ".vaultpass"

### create the root playbook
playbook_root.yml:
	sed -r 's/(become:).*/\1 no/' < playbook.yml > playbook_root.yml

### configure your run
configure: group_vars/all.yml .vaultpass

### generate group_vars
group_vars/all.yml:
	tools/configure

### remove the mess
clean:
	rm -f playbook_root.yml

mrproper: clean
	rm group_vars/all.yml
